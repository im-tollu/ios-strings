package iosstrings

case class IosString(key: String, text: String, comment: Option[String])
