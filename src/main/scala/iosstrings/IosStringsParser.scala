package iosstrings

import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers
import scala.util.parsing.input.Reader

object IosStringsParser extends RegexParsers {

  def parse(source: String): ParseResult[Seq[IosString]] = parse(iosStringSyntax, source)
  def parse(sequence: CharSequence): ParseResult[Seq[IosString]] = parse(iosStringSyntax, sequence)
  def parse(reader: Reader[Char]): ParseResult[Seq[IosString]] = parse(iosStringSyntax, reader)

  def iosStringSyntax: Parser[Seq[IosString]] = rep(iosString)

  def iosString: Parser[IosString] = {
    optNewLines ~> optSpace ~>
      (iosStringWithCommentBefore | iosStringWithInlineCommentAfter | iosStringWithoutComment) <~ newLine
  }

  def comment: Parser[Comment] = inlineComment | multilineComment

  override def skipWhitespace: Boolean = true

  override protected val whiteSpace: Regex = """[\t]+""".r

  protected case class KeyAndText(key: String, text: String)

  case class Comment(v: String)

  protected case class Literal(v: String)

  private def iosStringWithoutComment: Parser[IosString] = {
    keyAndText ^^ {
      case KeyAndText(key, text) => IosString(key, text, None)
    }
  }

  private def iosStringWithCommentBefore: Parser[IosString] = {
    comment ~ optNewLines ~ keyAndText ^^ {
      case comment ~ _ ~ keyAndText => IosString(keyAndText.key, keyAndText.text, Some(comment.v))
    }
  }

  private def iosStringWithInlineCommentAfter: Parser[IosString] = {
    keyAndText ~ optSpace ~ inlineComment ^^ {
      case keyAndText ~ _ ~ comment => IosString(keyAndText.key, keyAndText.text, Some(comment.v))
    }
  }

  private def keyAndText: Parser[KeyAndText] = {
    literal ~ equalitySign ~ literal <~ optSpace <~ ';' ^^ {
      case key ~ _ ~ text => KeyAndText(key.v, text.v)
    }
  }

  private def literal: Parser[Literal] = {
    '"' ~> """((.(?!"))*(.(?=")))?""".r <~ '"' ^^
      Literal
  }

  private def inlineComment: Parser[Comment] = {
    '/' ~> '/' ~> """((.(?!\n))*(.(?=\n)))?""".r ^^ {
      //    '/' ~> '/' ~> """.*$""".r ^^ {
      cs => Comment(cs.mkString.trim)
    }
  }

  private def multilineComment: Parser[Comment] = {
    '/' ~> '*' ~> """(?s)((.(?!\*/))*(.(?=\*/)))?""".r <~ '*' <~ '/' <~ optSpace ^^ {
      comment => Comment(comment.trim)
    }
  }

  private def equalitySign = optSpace ~ '=' ~ optSpace
  private def optNewLines = opt(rep(newLine))
  private def optSpace = opt(rep("""[ \t]+""".r))
  private val eoi = """\z""".r // end of input
  private val eol = sys.props("line.separator")
  private val newLine = eoi | eol

}
