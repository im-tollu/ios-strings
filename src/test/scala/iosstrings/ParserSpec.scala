package iosstrings

import iosstrings.IosStringsParser.Comment
import org.scalatest.{Assertion, FlatSpec, Matchers}

class ParserSpec extends FlatSpec with Matchers {

  "IosStringParser" should "parse inline comment" in {
    val source =
      """//a bit about a fox
        |
      """.stripMargin

    val expected = Comment("a bit about a fox")

    val result = IosStringsParser.parse(IosStringsParser.comment, source)

    shouldBeParsedAs(result, expected)
  }

  it should "parse multiline comment" in {
    val source =
      """/* Comment on It's ducks all the way
        |down. */
      """.stripMargin

    val expected = Comment("Comment on It's ducks all the way\ndown.")

    val result = IosStringsParser.parse(IosStringsParser.comment, source)

    shouldBeParsedAs(result, expected)
  }

  it should "parse iosString with inline comment before" in {
    val source =
      """
        |//a bit about a fox
        |"key-1" = "The [[quick]] brown fox jumped over the lazy dog.";
        |
      """.stripMargin

    val expected = IosString("key-1", "The [[quick]] brown fox jumped over the lazy dog.", Some("a bit about a fox"))

    val result = IosStringsParser.parse(IosStringsParser.iosString, source)

    shouldBeParsedAs(result, expected)
  }

  it should "parse iosString with multiline comment before" in {
    val source =
      """
        |
        |/* Comment on It's ducks all the way down. */
        |"key-3" = "It's ducks all the way down.";
        |
        |
      """.stripMargin

    val expected = IosString("key-3", "It's ducks all the way down.", Some("Comment on It's ducks all the way down."))

    val result = IosStringsParser.parse(IosStringsParser.iosString, source)

    shouldBeParsedAs(result, expected)
  }

  it should "parse iosString with inline comment after" in {
    val source =
      """
        |
        |"key-2" = "You don't look a gift horse \n In the mouth"; //it's a nice horse
        |
      """.stripMargin

    val expected = IosString("key-2", """You don't look a gift horse \n In the mouth""", Some("it's a nice horse"))

    val result = IosStringsParser.parse(IosStringsParser.iosString, source)

    shouldBeParsedAs(result, expected)
  }

  it should "parse iosString without comments" in {
    val source =
      """
        |
        |"key-0" = "";
        |
      """.stripMargin

    val expected = IosString("key-0", "", None)

    val result = IosStringsParser.parse(IosStringsParser.iosString, source)

    shouldBeParsedAs(result, expected)
  }

  it should "parse the whole example" in {
    val source =
      """
        |
        |//a bit about a fox
        |"key-1" = "The [[quick]] brown fox jumped over the lazy dog.";
        |
        |"key-2" = "You don't look a gift horse \n In the mouth" ; //it's a nice horse
        |
        |/* Comment on It's ducks all the way down. */
        |"key-3" =    "It's ducks all the way down.";
        |
        |"key-0" = "";
        |
        |"plural-key##{zero}" = "zero";
        |"plural-key##{one}" = "one";
        |"plural-key##{two}" = "two";
        |    "plural-key##{few}" = "few";
        |"plural-key##{some}" =   "many";
        |"plural-key##{other}" = "other";
        |
      """.stripMargin

    val expected = Seq(
      IosString("key-1", "The [[quick]] brown fox jumped over the lazy dog.", Some("a bit about a fox")),
      IosString("key-2", """You don't look a gift horse \n In the mouth""", Some("it's a nice horse")),
      IosString("key-3", "It's ducks all the way down.", Some("Comment on It's ducks all the way down.")),
      IosString("key-0", "", None),
      IosString("plural-key##{zero}", "zero", None),
      IosString("plural-key##{one}", "one", None),
      IosString("plural-key##{two}", "two", None),
      IosString("plural-key##{few}", "few", None),
      IosString("plural-key##{some}", "many", None),
      IosString("plural-key##{other}", "other", None)
      )

    val result = IosStringsParser.parse(IosStringsParser.iosStringSyntax, source)

    shouldBeParsedAs(result, expected)
  }

  def shouldBeParsedAs[T](result: IosStringsParser.ParseResult[T], t: T): Assertion = {
    result match {
      case IosStringsParser.Success(actual, _) => actual shouldEqual t
      case notSuccess => throw new RuntimeException(notSuccess.toString)
    }
  }
}
