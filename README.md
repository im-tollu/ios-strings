# iOS Strings Parser

While it is possible to write a lexer and a parser for this task, I used Scala 
[parser combinators](https://github.com/scala/scala-parser-combinators) library,
 which used to be a part of the Scala standard library, but currently is a separate
 community-maintained module.
 
 This project was build and tested with JDK 1.8, Scala 2.12.7 and Sbt 1.1.6
 
 To test the project use `sbt clean test` from the project's root
 
You can find example usage in a 
[Scala worksheet](https://bitbucket.org/im-tollu/ios-strings/src/master/src/main/scala/example.sc)